from fastapi import APIRouter

from app.api.routers import math_operations

api_router = APIRouter()
api_router.include_router(
    math_operations.router, prefix="/math_operations", tags=["Math Operations"]
)
