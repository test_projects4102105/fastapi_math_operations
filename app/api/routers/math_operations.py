import logging
from typing import List

from fastapi import APIRouter, HTTPException

from app.api.exceptions import RedisAllMathOperationTasksError
from app.api.utils import MathOperation, MathOperationTaskResponse
from app.tasks.tasks import (
    get_all_math_operation_tasks,
    get_task_result,
    run_math_operation,
)

router = APIRouter()


@router.post("/operations", status_code=201)
def add_operation(operation: MathOperation) -> str:
    """
    :param operation:

    :return: id of background task thah makes calculations
    """
    try:
        res = run_math_operation.delay(
            operation.x, operation.y, operation.operator.value
        )
        return res.id  # type: ignore
    except Exception as error:
        logging.error(error)
        raise HTTPException(status_code=500, detail="Unable to make operation")


@router.get("/operations/{task_id}")
def get_operation_result(task_id: str) -> int:
    """
    :param task_id: UUID string representing id for task within Celery
    :return: result of task with specified task_id. If task_id is invalid, returns None
    """
    try:
        res = get_task_result(task_id)
    except ArithmeticError:
        raise HTTPException(status_code=400, detail="Cannot divide by zero")
    if res is None:
        raise HTTPException(
            status_code=202, detail="The calculation is currently in progress"
        )
    return res


@router.get("/operations")
def get_all_operations() -> List[MathOperationTaskResponse]:
    """
    :return: list of all tasks related to Math Operation with its id and state
    """
    try:
        all_tasks = get_all_math_operation_tasks()
        return all_tasks
    except RedisAllMathOperationTasksError:
        raise HTTPException(status_code=500)
