from enum import Enum
from typing import Union
from uuid import UUID

from pydantic import BaseModel


class Operator(Enum):
    add = "+"
    multiply = "*"
    subtract = "-"
    divide = "/"


class CeleryTaskResult(Enum):
    pending = "PENDING"
    success = "SUCCESS"
    failure = "FAILURE"


class MathOperation(BaseModel):
    x: Union[float, int]
    y: Union[float, int]
    operator: Operator


class MathOperationTaskResponse(BaseModel):
    id: UUID
    status: CeleryTaskResult
