from fastapi import FastAPI

from app.api.api_ import api_router

app = FastAPI()

app.include_router(api_router)
