# mypy: ignore-errors
from celery import Celery
from redis import Redis

from app.config import REDIS_HOST, REDIS_PORT

redis_app = Redis(host=REDIS_HOST, port=REDIS_PORT)
celery_app = Celery("tasks")
celery_app.config_from_object("app.tasks.celeryconfig")
