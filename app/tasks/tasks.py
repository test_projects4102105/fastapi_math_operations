import json
import logging
from enum import Enum
from itertools import chain
from time import sleep
from typing import List, Optional, Union

from celery.exceptions import TimeoutError
from celery.result import AsyncResult

from app.api.exceptions import RedisAllMathOperationTasksError
from app.api.utils import CeleryTaskResult, MathOperationTaskResponse
from app.config import CELERY_RESULT_TIMEOUT
from app.setup import celery_app, redis_app


class Operator(Enum):
    add = "+"
    multiply = "*"
    subtract = "-"
    divide = "/"


operations_mapper = {
    "+": lambda x, y: x + y,
    "*": lambda x, y: x * y,
    "-": lambda x, y: x - y,
    "/": lambda x, y: x / y,
}


@celery_app.task(bind=True)
def run_math_operation(self, x: Union[float, int], y: Union[float, int], operator: str) -> None:  # type: ignore
    """
    :param x: first argument of mathematical expression
    :param y: second argument of mathematical expression
    :param operator: mathematical operator for x and y
    :return: calculated number or None if failed to compute
    """
    task_id = self.request.id
    pending_info = json.dumps({"id": task_id, "status": CeleryTaskResult.pending.value})
    try:
        redis_app.lpush("celery_pending_tasks", pending_info)
        # calculation
        res = operations_mapper[operator](x, y)
        # remove pending task from redis and add it to list of sucessful tasks
        redis_app.lrem("celery_pending_tasks", 1, pending_info)
        redis_app.lpush(
            "celery_successfull_tasks",
            json.dumps({"id": task_id, "status": CeleryTaskResult.success.value}),
        )
        return res
    except ZeroDivisionError:
        logging.error("Celery: Cannot divide by zero")
        # remove pending task from redis and add it to list of failed tasks
        redis_app.lrem("celery_pending_tasks", 1, pending_info)
        redis_app.lpush(
            "celery_failed_tasks",
            json.dumps({"id": task_id, "status": CeleryTaskResult.failure.value}),
        )
        raise ZeroDivisionError
    except:
        logging.error("Task % failed to execute", task_id)
        # remove pending task from redis and add it to list of failed tasks
        redis_app.lrem("celery_pending_tasks", 1, pending_info)
        redis_app.lpush(
            "celery_failed_tasks",
            json.dumps({"id": task_id, "status": CeleryTaskResult.failure.value}),
        )


def get_task_result(task_id: str) -> Optional[int]:
    """

    :param task_id: UUID of task
    :return: calculated number or None if failed to compute
    """
    try:
        res: Optional[int] = AsyncResult(task_id, app=celery_app).get(
            timeout=CELERY_RESULT_TIMEOUT
        )
        if res is None:
            raise ArithmeticError
        return res
    except TimeoutError as error:
        logging.error(error)
        return None


def get_all_math_operation_tasks() -> List[MathOperationTaskResponse]:
    """
    :return: all (pending, successful and failed) tasks from redis
    """
    try:
        celery_pending_tasks = redis_app.lrange("celery_pending_tasks", start=0, end=-1)
        celery_successfull_tasks = redis_app.lrange(
            "celery_successfull_tasks", start=0, end=-1
        )
        celery_failed_tasks = redis_app.lrange("celery_failed_tasks", start=0, end=-1)

        all_tasks = [
            json.loads(task)
            for task in chain(
                celery_pending_tasks,
                celery_successfull_tasks,
                celery_failed_tasks,
            )
        ]
        return all_tasks
    except Exception as error:
        logging.error(error)
        raise RedisAllMathOperationTasksError
