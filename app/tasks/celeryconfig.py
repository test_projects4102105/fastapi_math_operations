from app.config import REDIS_HOST

broker_url = f"redis://{REDIS_HOST}:6379"
result_backend = f"redis://{REDIS_HOST}:6379"
imports = ["app.tasks.tasks"]
