import pytest

from app.config import CELERY_RESULT_TIMEOUT
from app.tasks.tasks import run_math_operation


@pytest.mark.celery(task_always_eager=True)
@pytest.mark.parametrize(
    "x, y, operator, expected",
    [
        (1, 2, "+", 3),
        (4, 2, "/", 2),
        (6, 3, "-", 3),
        (4, 6, "*", 24),
        (2, 0, "/", ZeroDivisionError),
    ],
)
def test_run_math_operation(celery_app, celery_worker, x, y, operator, expected):
    if type(expected) == type and issubclass(expected, Exception):
        with pytest.raises(expected):
            run_math_operation.delay(x, y, operator).get(timeout=CELERY_RESULT_TIMEOUT)
    else:
        assert (
            run_math_operation.delay(x, y, operator).get(timeout=CELERY_RESULT_TIMEOUT)
            == expected
        )
