from contextlib import nullcontext as does_not_raise
from uuid import UUID

import pytest

from app.tests.test_math_operation.conftest import client


@pytest.mark.parametrize(
    "x, y, operator",
    [
        (1, 2, "+"),
        (4, 2, "/"),
        (6, 3, "-"),
        (4, 6, "*"),
        (2, 0, "/"),
    ],
)
def test_add_operation(x, y, operator):
    response = client.post(
        "/math_operations/operations",
        json={
            "x": x,
            "y": y,
            "operator": operator,
        },
    )
    assert response.status_code == 201
    with does_not_raise():
        UUID(response.json())


@pytest.fixture
def successful_operation_id():
    response = client.post(
        "/math_operations/operations",
        json={
            "x": 1,
            "y": 2,
            "operator": "+",
        },
    )
    print("SUCCESSFUL::::", response.json())
    return response.json()


def test_successful_get_operation_result(successful_operation_id):
    response = client.get(f"/math_operations/operations/{successful_operation_id}")
    assert response.json() == 3


@pytest.fixture
def unsuccessful_operation_id():
    response = client.post(
        "/math_operations/operations",
        json={
            "x": 1,
            "y": 0,
            "operator": "/",
        },
    )
    return response.json()


def test_unsuccessful_get_operation_result(unsuccessful_operation_id):
    response = client.get(f"/math_operations/operations/{unsuccessful_operation_id}")
    assert response.status_code == 400
    assert response.json() == {"detail": "Cannot divide by zero"}


def test_abc(multiple_math_operations):
    print(multiple_math_operations)
    response = client.get(f"/math_operations/operations")
    for operation in multiple_math_operations:
        assert operation in response.json()
