import pytest
from starlette.testclient import TestClient

from app.config import REDIS_HOST, REDIS_PORT
from app.main import app

pytest_plugins = ("celery.contrib.pytest",)


@pytest.fixture(scope="session")
def celery_config():
    return {
        "broker_url": f"redis://{REDIS_HOST}:{REDIS_PORT}",
        "result_backend": f"redis://{REDIS_HOST}:{REDIS_PORT}",
    }

client = TestClient(app=app)

@pytest.fixture(scope="session")
def multiple_math_operations():
    created_tasks_ids = []
    test_data = [
        (1, 2, "+", "SUCCESS"),
        (4, 2, "/", "SUCCESS"),
        (6, 3, "-", "SUCCESS"),
        (4, 6, "*", "SUCCESS"),
        (2, 0, "/", "FAILURE"),
    ]
    for x, y, operator, state in test_data:
        response = client.post(
            "/math_operations/operations",
            json={
                "x": x,
                "y": y,
                "operator": operator,
            },
        )
        created_tasks_ids.append({"id": response.json(), "status": state})
    return created_tasks_ids
