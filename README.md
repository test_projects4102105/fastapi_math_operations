# Math Operations
This project is intended for complex computations using two numbers and operator (`+`, `-`, `*` or `/`).

## Images
Swagger
![Swagger](images/swagger_1.jpg)
![Swagger](images/swagger_2.jpg)
Flower
![Flower](images/flower.jpg)

## Run locally
To run locally, at first `git clone` this repo and then run `docker compose up` to run all containers (FastAPI, Redis, Celery worker, Flower worker).

## Usage
At first, go to `http://localhost:8014/docs` to enter the [Swagger documentation](https://swagger.io/docs/) of the application.
1. User sends two numbers and operator by using `POST /math_operations/operations` request and receives the id of the task
2. To check the result of the calculation one needs to make a `GET /math_operations/operations/<task_id>` request with the `task_id` parameter received on the previous step. One will receive a calculated number, message with HTTP code 400 when dividing by zero or response 202 if the calculation has not yet been finished.
3. To check all tasks with their id and state, simply `GET /math_operations` to see the list of mappings with keys `id` and `status`.

## Monitoring
Go to `http://localhost:5557` to open Flower monitoring tool and go on the `Tasks` tab to see all of the tasks with the information about their state, received arguments, result, runtime and more.

## Testing
Code is covered with unit tests with the help of `pytest`. Currently all endpoints and celery tasks are covered with tests. To run test, at first change variable `REDIS_HOST` in `config.py` to appropriate ip-address, e.g. `localhost` and then run `python3 -m pytest -s -v app/tests/`.

## Autoformatting
`Black`, `mypy` and `isort` are used to keep the high quality of the code.

## Miscellaneous
Celery is used to run background tasks. Redis is used as celery broker and result backend that stores results of all calculations.